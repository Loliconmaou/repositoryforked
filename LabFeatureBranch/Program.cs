﻿using System;

namespace LabFeatureBranch
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int Num;
            EnterNum:
            if (!Int32.TryParse(Console.ReadLine(), out Num))
            {
                Console.WriteLine("Not a number!");
                goto EnterNum;
            }
            Console.Write("\n\n");
            Console.Write("Natural number of: "+Num+"\n");
            Console.Write("---------------------------------------");
            Console.Write("\n\n");
            Console.WriteLine("The natural number are:");
            for (int i = 1; i <= Num;i++) 
            {
                Console.Write("{0} ", i);
            }
            Console.Write("\n\n");
        }
    }
}
